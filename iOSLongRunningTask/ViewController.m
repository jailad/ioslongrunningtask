//
//  ViewController.m
//  iOSLongRunningTask
//
//  Created by Jai on 6/18/15.
//  Copyright (c) 2015 Jai. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (IBAction)doALongRunningOperationOnMainThread:(id)sender{
    NSLog(@"Started a long running operation on main thread");
    NSDate *start = [NSDate date];
    [self task];
    NSDate *methodFinish = [NSDate date];
    NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:start];
    NSLog(@"Finished a long running operation on main thread in: %f seconds", executionTime);
}

- (IBAction)doALongRunningOperationOnBackgroundThread:(id)sender{
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        NSLog(@"Started Long Running operation on background thread.");
        [self task];
        NSLog(@"Finished a Long Running operation on background thread.");
    });
}

-(void)task{
    for(int i = 0; i<1000000; i++){
        // No - op
        for(int i = 0; i<1200; i++){
            // No - op
        }
    }
}


@end
