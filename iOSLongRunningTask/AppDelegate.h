//
//  AppDelegate.h
//  iOSLongRunningTask
//
//  Created by Jai on 6/18/15.
//  Copyright (c) 2015 Jai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

